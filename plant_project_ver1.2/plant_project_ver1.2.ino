/*
  @ Starting Date : 21.05.2019
  @ User : Chameera@UoM (chameeracjw@gmail.com)
  @ Bugs : Not Known Bugs
  @ Brief : This is the firmware of Plant Project.
  @ Des : This firmware is com[atible with ESP8266(E-12) Nodemcu.
  @ This firmware measure pH value and moisture value of soil and push them into a user specified location.
  @ Finalized Date :25.05.2019
  @ Firmware Version : 1.2
*/

const char* ssid = "Chameera_UoM"; // SSID of AP
const char* password = "19960510"; // password

// Libraries to use
#include <ESP8266HTTPClient.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>

#define indicator D3 // LED notification pin
#define mode_pin D4 //this pin toggles input to A0 (HIGH -> pH, LOW -> Moisture)

bool is_connected = false;
bool measure_now = true;
bool published = false;

uint16_t min_in_millis = 60000;
uint32_t pre_measure_millis;
uint8_t measure_interval = 2; // measure interval in minutes
uint8_t min_from_last_measure; //minutes elapsed from the last measure 

uint32_t pre_connect_millis;
uint8_t connect_interval = 1; // reconnect attemp interval

uint32_t pre_indicate_millis;
uint32_t indicate_interval = 500; // Indicator blinking interval in miliseconds

int push_message(String pH_val, String moisture_val); // publish message to server
bool measure_pH(void); // measure ph value
bool measure_moisture(void); // measure mosture value
void eeprom_to_server(void); // publish data from eeprom to server
void save_data(void); // save measured data from sensors

bool values_in_eeprom = false;
uint8_t eeprom_half_size = 64;
uint8_t eeprom_ph_addr = 0;
uint8_t eeprom_moisture_addr = eeprom_half_size;

float last_ph = 0;
float last_moisture = 0;

void setup()
{
  Serial.begin(115200); // Begin serial monitor

  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  
  Serial.println("\n");
  Serial.println("Welcome to Soil pH and Moisture Measurer");
  
  WiFi.begin(ssid, password); // IFrst attemp to connect to a WiFi network
  delay(5000);

  EEPROM.begin(2 * eeprom_half_size); // begin EEPROM to save data
  delay(2000);

  if (WiFi.status() == WL_CONNECTED) {  // If connected to a wifi netwirk during first atempt
    Serial.println("Connected to a WiFi network...!!!");
    is_connected = true;
    eeprom_to_server(); // send already saved data from previous session to server
  }


  else {
    for (uint8_t i = 0; i < eeprom_half_size; i++ ) { // if eeprom already have data address should start at the end of them
      if (EEPROM.read(i) == 0) {
        eeprom_ph_addr = i;
        eeprom_moisture_addr = i + eeprom_half_size;
        break;
      }
      else {
        values_in_eeprom = true;
      }
    }
    Serial.println("Initial attempt to connect is failed..!!!");
  }
}

void loop()
{
  if (is_connected && !values_in_eeprom) {
    digitalWrite(indicator, HIGH);
  }
  else {
    if (millis() - pre_indicate_millis > indicate_interval) { //counting minutes
      digitalWrite(indicator, !digitalRead(indicator));
      pre_indicate_millis = millis();

    }
  }
  
  if (millis() - pre_connect_millis > connect_interval * min_in_millis) { //check connectivity
    pre_connect_millis = millis();

    if (WiFi.status() != WL_CONNECTED) { // wifi is not connected
      is_connected = false;
      Serial.println("WiFi is not connected. Retrying to connect...!!!");
      WiFi.begin(ssid, password); //retry to connect
      delay(5000);
    }

    if (WiFi.status() == WL_CONNECTED) { // if wifi connected in last attempt
      is_connected = true;
      Serial.println("WiFi Connected");
      eeprom_to_server(); //upload data from EEPROM to server
    }
  }

  if (millis() - pre_measure_millis > min_in_millis) { //counting minutes
    min_from_last_measure++;
    pre_measure_millis = millis();
    Serial.print(min_from_last_measure);
    Serial.println(" minute(s) has elapsed since last measure");
  }

  if (min_from_last_measure == measure_interval) { // check elepased time equal to measure interval in minutes
    measure_now = true;
    min_from_last_measure = 0;
    Serial.println("Measuring starts now");
  }

  while (measure_now) { // starts measuring
    if (measure_pH() && measure_moisture()) { //if both are measured
      measure_now = false; // measuring loop stops
      save_data();
    }
  }
}
