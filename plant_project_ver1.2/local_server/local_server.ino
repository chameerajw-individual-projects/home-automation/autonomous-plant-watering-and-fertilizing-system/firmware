#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

// Replace with your network credentials
const char* ssid = "Chameera_UoM";
const char* password = "19960510";

IPAddress ip(192, 168, 8, 111); //set static ip
IPAddress gateway(192, 168, 8, 1); //set gateway
IPAddress subnet(255, 255, 255, 0);//set subnet

ESP8266WebServer server(80);   //instantiate server at port 80 (http port)

String page = "";
double pH_value, moisture_value; 
void setup(void){

  Serial.begin(115200);
  delay(5000);
  WiFi.config(ip, gateway, subnet);
  WiFi.begin(ssid, password); //begin WiFi connection
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  
  server.on("/", [](){
    page = "<h1>Custom Webserver for Plant Project</h1>" "<h3>pH Reading: "+String(pH_value)+ "<h3>Moisture Reading: " +String(moisture_value);
    server.send(200, "text/html", page);
  });
  
  server.begin();
  Serial.println("Web server started!");

  Serial.print("Type this address in URL to connect: ");
  Serial.print("http://");
  Serial.print(ip);
  Serial.println("/");
}
 
void loop(void){
  pH_value = 7.95;
  moisture_value = 12.23;
  delay(1000);
  server.handleClient();
}
