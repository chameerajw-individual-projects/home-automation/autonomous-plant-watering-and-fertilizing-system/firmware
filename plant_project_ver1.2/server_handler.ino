int push_message(String pH_val, String moisture_val) {
  published = false;
  HTTPClient http;
  String url = "http://enigmalk.com/test.php?ph=" + pH_val + "&moist=" + moisture_val;
  String url = "http://myfirstplantproject.000webhostapp.com/writefile.php?pH=" + pH_val + "&Moisture=" + moisture_val + "%";

  http.begin(url);
  http.addHeader("Content-Type", "text/plain");
  int httpCode = http.GET();
  String payload = http.getString();
  if ( payload == "SUCCESS. Data written in file.") {
    published = true;
    is_connected = true;
    Serial.println("Published Succesfully");
  }
  else {
    is_connected = false;
    Serial.println("Publishing failed");
  }
  //Serial.println("While sending I received this from server : " + payload);
  http.end();
}

void eeprom_to_server(void) {
  if (EEPROM.read(0) != 0) { // if data in EEPROM
    values_in_eeprom = true;

    for (uint8_t addr = 0; addr < eeprom_half_size; addr += sizeof(uint8_t)) {
      if (uint8_t(EEPROM.read(addr)) == 0) { // if data in EEPROM is over
        Serial.println("EEPROM data is over");
        values_in_eeprom = false;
        break; // go back
      }
      Serial.print("Uploading data in EEPROM address: ");
      Serial.print(addr);
      Serial.print("   Data: ");
      Serial.println(EEPROM.read(addr));
      push_message (String(float(EEPROM.read(addr))), String(float(EEPROM.read(addr + eeprom_half_size)))); //upload them to the cloud
      if (!published) {
        Serial.println("Error in internet connection");
        break;
      }
    }

    if (published) {
      for (uint8_t addr = 0; addr < eeprom_half_size * 2 ; addr++) { //clear eeprom
        EEPROM.write(addr, 0);
        EEPROM.commit();
      }
      eeprom_ph_addr = 0;
      eeprom_moisture_addr = eeprom_half_size;
      values_in_eeprom = false;
      Serial.println("Clearing EEPROM");
    }

  }
}

void save_data(void) {
  if (WiFi.status() == WL_CONNECTED) {//if wifi connected
    is_connected = true;
    Serial.println("Pushing values to the Server");
    push_message (String(last_ph), String(last_moisture)); // upload data to the cloud
  }

  if (!published || WiFi.status() != WL_CONNECTED) { // wifi data is published
    is_connected = false;
    Serial.println("Pushing values to the EEPROM");
    EEPROM.write(eeprom_ph_addr, uint8_t(round(last_ph)));
    delay(100);
    EEPROM.write(eeprom_moisture_addr, uint8_t(round(last_moisture)));
    delay(100);
    EEPROM.commit(); //push value to the eeprom
    delay(100);
    Serial.print("Saving data in EEPROM address: ");
    Serial.print(eeprom_ph_addr);
    Serial.print("   Data: ");
    Serial.println(EEPROM.read(eeprom_ph_addr));

    eeprom_ph_addr += sizeof(uint8_t);
    eeprom_moisture_addr += sizeof(uint8_t);
    values_in_eeprom = true;

    if (eeprom_ph_addr == eeprom_half_size - 1) { // if eeprom filled ring it again
      Serial.println("EEPROM_filled");
      eeprom_ph_addr = 0;
      eeprom_moisture_addr = 0;
    }
  }
}

