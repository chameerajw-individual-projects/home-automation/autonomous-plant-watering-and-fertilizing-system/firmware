#include <EEPROM.h>

#include<ESP8266WiFi.h>

void setup() {
  pinMode(D4, OUTPUT);
  Serial.begin(115200);
  // put your setup code here, to run once:
  EEPROM.begin(32);
  //EEPROM.write(1,12.25);
  //EEPROM.commit();
}

void loop() {
  //  digitalWrite(D4, LOW);
  //  Serial.print("OFF: ");
  //  Serial.println(analogRead(A0));
  //  delay(1000);
  //  digitalWrite(D4, HIGH);
  //  Serial.print("ON: ");
  //  Serial.println(analogRead(A0));
  //  //Serial.println(analogRead(A0)*3.3*3.5/1000);
  //  delay(500);
  // put your main code here, to run repeatedly:
  float var = EEPROM.read(1);
  Serial.println(var);
}
