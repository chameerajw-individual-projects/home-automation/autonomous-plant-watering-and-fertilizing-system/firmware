bool measure_pH(void) {
  float temp_val = 0;
  uint8_t no_of_samples = 50; // this should be less than 63(= floor(65535/1023))
  float  offset = 0;
  digitalWrite(mode_pin, HIGH); // toggle ph input to A0
  Serial.println("Measuring ph value");

  for (uint8_t times = 0; times < no_of_samples; times++) {
    temp_val += analogRead(A0); // integrating samples
    delay(100);
    digitalWrite(indicator, !digitalRead(indicator));// toggle indicator
  }

  last_ph =14 - ((temp_val / no_of_samples) * 3.5 * (3.3 / 1023)) + offset; // averaging and mapping
  Serial.print("Measured pH value: ");
  Serial.println(last_ph);
  return true;
}

