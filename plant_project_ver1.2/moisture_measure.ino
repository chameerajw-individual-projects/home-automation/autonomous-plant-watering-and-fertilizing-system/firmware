bool measure_moisture(void) {
  float wet_level = 0.00;
  float dry_level = 1023.00;

  float healthy_wet_level = 0.00;
  float healthy_dry_level = 100.00;

  float temp_val;
  
  digitalWrite(mode_pin, LOW); // toggle moisture input to A0
  Serial.println("Measuring moisture value");
  temp_val = analogRead(A0);
  
  float mapped = map(temp_val, wet_level, dry_level, healthy_wet_level, healthy_dry_level); // mappin voltage value to percentage
  
  last_moisture = 100.00- mapped;
  Serial.print("Measured moisture value: ");
  Serial.println(last_moisture);

  return true;
}

